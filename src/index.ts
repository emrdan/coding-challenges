import { program } from 'commander';
import { random } from './commands/random';
import { prime } from './commands/prime';
import { fibonacci } from './commands/fibonacci';
import { cashback } from './commands/atm';

program
  .command('random [quantityToGenerate]')
  .description('Generate a list of random numbers. Max 20.')
  .action(random);

program
  .command('prime [quantityToGenerate]')
  .description('Generate a list of prime numbers. Default 9.')
  .action(prime);

program
  .command('fibonacci [N]')
  .description('Builds a fibonnaci sequence as long as N.')
  .action(fibonacci)

program
  .command('cashback <number>')
  .description('Decompose a number into bills and coins.')
  .action(cashback)

program.parse();