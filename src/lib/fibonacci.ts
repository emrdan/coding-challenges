export class FibonacciCalculator {
  calculateNthSquence(upTo: number): number[] {
    const sequence: number[] = upTo <= 2 ? [] : [0, 1];
  
    if (upTo <= 2) {
      for (let i = 0; i < upTo; i++) { sequence.push(i) }
    } else {
      for (let i = 2; i < upTo; i++) {
        let fNth = sequence[i - 2] + sequence[i - 1];
        sequence.push(fNth);
      }
    }
  
    return sequence;
  }
}