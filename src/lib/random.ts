import { randomInt } from 'crypto';

export class RandomNumberGenerator {
  private MAX_QUANTITY: number = 20;
  private DEFAULT_QUANTITY: number = 5;
  private MIN_VALUE: number = 1;
  private MAX_VALUE: number = 100;
  
  private generateRandomNumber(min: number, max: number): number {
    return randomInt(min, max + 1);
  }

  generateRandomNumbers(quantityToGenerate: number = this.DEFAULT_QUANTITY): Array<number> {
    const numbers: number[] = [];
  
    quantityToGenerate = quantityToGenerate > this.MAX_QUANTITY ? this.MAX_QUANTITY : quantityToGenerate;
  
    while (numbers.length < quantityToGenerate) {
      let generatedNumber = this.generateRandomNumber(this.MIN_VALUE, this.MAX_VALUE);
  
      if (!numbers.includes(generatedNumber)) { /* Set is an alternative */
        numbers.push(generatedNumber);
      }
    }
  
    return numbers;
  }
};