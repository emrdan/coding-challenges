export class PrimeNumberGenerator {
  private DEFAULT_QUANTITY: number = 9;

  private isAPrimeNumber(n: number): boolean {
    let isPrime: boolean = true;
  
    for (let i = 2; i < n; i++) {
      if (n % i === 0) {
        isPrime = false;
        return isPrime;
      }
    }
  
    return isPrime;
  }

  generatePrimeNumbers(quantityToGenerate: number = this.DEFAULT_QUANTITY): number[] {
    const primeNumbers: number[] = [];
  
    for(let i = 2; primeNumbers.length < quantityToGenerate; i++) {
      if(this.isAPrimeNumber(i)) {
        primeNumbers.push(i);
      }
    }
  
    return primeNumbers;
  }
}