type Cash = 'bills' | 'coins';

type DenominationCount = {
  denomination: number,
  count: number
}

type CashTypeCount = {
  bills: number,
  coins: number
}

export class ATMEmulator {
  private bills: number[] = [2000, 1000, 500, 200, 100, 50];
  private coins: number[] = [25, 10, 5, 1];

  calculateCashPerDenomination(value: number): DenominationCount[] {    
    let billsAndCoinsThatApply: DenominationCount[] = this.excludeBillsAndCoinsThatDontApply(value);
    let dividend: number = value;
  
    for (let i = 0; i < billsAndCoinsThatApply.length; i++) {
      let cash = billsAndCoinsThatApply[i].denomination;
      let quotient = dividend / cash;

      billsAndCoinsThatApply[i].count = Math.floor(quotient)
             
      if (Number.isInteger(quotient)) {   
        break;
      } else {
        let remainder = dividend % cash;
        dividend = remainder;
      }
    }

    return billsAndCoinsThatApply.filter((denomination) => denomination.count > 0);
  }

  calculateCashPerType(n: number): CashTypeCount {
    const cashPerDenomination: DenominationCount[] = this.calculateCashPerDenomination(n);
    const cashPerType: CashTypeCount = { bills: 0, coins: 0 };

    for (let i = 0; i < cashPerDenomination.length; i++) {
      let cashType = this.getCashType(cashPerDenomination[i].denomination);
      cashPerType[cashType] = cashPerType[cashType] + cashPerDenomination[i].count;
    }

    return cashPerType;
  }

  private excludeBillsAndCoinsThatDontApply (n: number): DenominationCount[] {
    const allCash = [...this.bills, ...this.coins];
    const applicableCash = (cash: number) => cash <= n;
    
    return allCash
      .filter(applicableCash)
      .map((cash) => { 
        return { 
          denomination: cash, 
          count: 0 
        }
      });
  }

  private getCashType(n: number): Cash {
    return n > 25 ? 'bills' : 'coins';
  }
}