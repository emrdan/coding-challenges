import { PrimeNumberGenerator } from "../lib/prime";

export function prime(quantityToGenerate = 9) {
  console.log(new PrimeNumberGenerator().generatePrimeNumbers(quantityToGenerate));

};