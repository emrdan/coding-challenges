import { ATMEmulator } from "../lib/atm";

export function cashback(n: number) {
  const emulator = new ATMEmulator();
  
  console.log('Quantity of bills and coins: ', emulator.calculateCashPerType(n));
  console.log('Quantity per denomination: ', emulator.calculateCashPerDenomination(n));
};