import { FibonacciCalculator } from "../lib/fibonacci";

export function fibonacci(upTo = 5) {
  const calculator = new FibonacciCalculator();
  
  console.log(calculator.calculateNthSquence(upTo));
};