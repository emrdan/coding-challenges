import { RandomNumberGenerator } from "../lib/random";

export function random(quantityToGenerate = 5) {
  console.log(new RandomNumberGenerator().generateRandomNumbers(quantityToGenerate));
};