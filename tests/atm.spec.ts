/**
 * 
 * ATM Machine Emulator for Dominican Republic
 * ============================================
 * a) Calculate the amount of bills and coins contained 
 *    in a value (eg.: 305 should be 2 bills, 1 coin)
 * 
 * b) Calculate the amount of bills and coins per denomi
 *    nation (eg.: 305 should be 1 of 200, 1 of 100 and 1 of 5)
 * 
 * Denominations in DR are the following:
 * 
 *    bills: 2000, 1000, 500, 200, 100, 50
 *    coins: 25, 10, 5, 1
 * 
 * Source: Interview
 * Solved and Tested by Daniel Méndez
 */

import { expect } from 'chai';
import { ATMEmulator } from '../src/lib/atm';

describe("[Feature] ATM Emulator for Dominican Republic", function() {
  const emulator = new ATMEmulator();

  describe("Calculates quantity of bills and coins", function () {
    it(`calculatePerCashType(305) should equal 
      { 'bills': 2, 
        'coins': 1
      }`, function () {
        expect(emulator.calculateCashPerType(305)).deep.equal({
          bills: 2,
          coins: 1
        });
    })
  })

  describe("Calculates quantity of bills and coins per denomination", function () {
    it(`calculatePerCashType(305) should equal 
      [{ denomination: 200, 
        count: 1
      }, {
        denomination: 100,
        count: 1
      }, {
        denomination: 5,
        count: 1
      }]`, function () {
      expect(emulator.calculateCashPerDenomination(305)).deep.equal([{
        denomination: 200,
        count: 1
      }, {
        denomination: 100,
        count: 1
      }, {
        denomination: 5,
        count: 1
      }]);
    })
  })
});