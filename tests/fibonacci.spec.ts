/**
 * 
 * Fibonacci Sequence Generator
 * =============================
 * a) Calculate the Fibonacci Sequence up to N
 * 
 * eg.: 5 => [0, 1, 1, 2, 3]
 * 
 * Source: Interview
 * Solved and Tested by Daniel Méndez
 */

import { expect } from 'chai';
import { FibonacciCalculator } from '../src/lib/fibonacci';

describe("[Feature] Fibonacci Calculator", function() {
  const calculator = new FibonacciCalculator();
  
  describe("Calculates the fibonacci sequence up to N", function () {
    it("calculateNthSquence(1) should generate [0]", function () {
      expect(calculator.calculateNthSquence(1)).to.deep.equal([0]);
    })

    it("calculateNthSquence(2) should generate [0, 1]", function () {
      expect(calculator.calculateNthSquence(2)).to.deep.equal([0, 1]);
    })

    it("calculateNthSquence(3) should generate [0, 1, 1]", function () {
      expect(calculator.calculateNthSquence(3)).to.deep.equal([0, 1, 1]);
    })

    it("calculateNthSquence(4) should generate [0, 1, 1, 2]", function () {
      expect(calculator.calculateNthSquence(4)).to.deep.equal([0, 1, 1, 2]);
    })
  })
});