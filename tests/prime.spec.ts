/**
 * 
 * Prime Numbers Generator
 * ==========================
 * a) Generate a list of prime numbers
 * b) Quantity should be 9 by default
 * 
 * eg.: 
 *  default => [2, 3, 5, 7, 11, 13, 17, 19, 23]
 *  5 => [2, 3, 5, 7, 11]
 * 
 * Source: Interview
 * Solved and Tested by Daniel Méndez
 */

import { expect } from 'chai';
import { PrimeNumberGenerator } from '../src/lib/prime';

describe("[Feature] Prime Numbers Generator", function() {
  const generator = new PrimeNumberGenerator();
  
  describe("Generates 9 prime numbers by default", function () {
    it("generatePrimeNumbers() should generate [2, 3, 5, 7, 11, 13, 17, 19, 23]", function () {
      const numbers: number[] = generator.generatePrimeNumbers();
      expect(numbers).to.deep.equal([2, 3, 5, 7, 11, 13, 17, 19, 23])
    })
  })
  
  describe("Generates N prime numbers", function () {
    it("generatePrimeNumbers(2) should generate [2, 3]", function() {
      const numbers: number[] = generator.generatePrimeNumbers(2);
      expect(numbers).to.deep.equal([2, 3])
    })

    it("generatePrimeNumbers(3) should generate [2, 3, 5]", function() {
      const numbers: number[] = generator.generatePrimeNumbers(3);
      expect(numbers).to.deep.equal([2, 3, 5])
    })

    it("generatePrimeNumbers(4) should generate [2, 3, 5, 7]", function() {
      const numbers: number[] = generator.generatePrimeNumbers(4);
      expect(numbers).to.deep.equal([2, 3, 5, 7])
    })

    it("generatePrimeNumbers(5) should generate [2, 3, 5, 7, 11]", function() {
      const numbers: number[] = generator.generatePrimeNumbers(5);
      expect(numbers).to.deep.equal([2, 3, 5, 7, 11])
    })

  })

});