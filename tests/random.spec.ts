/**
 * 
 * Random Numbers Generator
 * ==========================
 * a) Quantity should be 5 by default
 * b) Quantity should max out at 20
 * c) The least possible value generated in the list should be 1
 * d) The greatest possible value generated in the list should be 100
 * e) Numbers in the list should not be repeated
 * 
 * eg.: 
 *  default => [5, 2, 9, 3, 99]
 *  7 => [3, 1, 8, 5, 7, 9, 100]
 *  100 => [3, 6, 9, 2, 1, 
 *          99, 55, 71, 34, 64, 
 *          86, 100, 22, 45, 39, 
 *          23, 66, 83, 17, 15]
 * 
 * Source: Interview
 * Solved and Tested by Daniel Méndez
 */

import { expect } from 'chai';
import { RandomNumberGenerator } from '../src/lib/random';

describe("[Feature] Random Numbers Generator", function() {
  const generator = new RandomNumberGenerator();
  
  describe("Generates 5 unique random numbers by default", function() {
    it("generateRandomNumbers() should generate 5 random numbers", function() {
      const numbers: number[] = generator.generateRandomNumbers();
      const uniqueNumbers: number[] = numbers.filter((value, index, array) => array.indexOf(value) === index);

      expect(numbers).to.be.an('array').of.length(5);
      expect(numbers.length).to.equal(uniqueNumbers.length);
    });
  })

  describe("Generates N (up to 20) unique random numbers", function() {
    it("generateRandomNumbers(10) should generate 10 random numbers", function() {
      const quantity: number = 10;
      const numbers: number[] = generator.generateRandomNumbers(quantity);
      const uniqueNumbers: number[] = numbers.filter((value, index, array) => array.indexOf(value) === index);

      expect(numbers.length).to.equal(uniqueNumbers.length);
      expect(numbers).to.be.an('array').of.length(quantity);
    });

    it("generateRandomNumbers(15) should generate 15 random numbers", function() {
      const quantity: number = 15;
      const numbers: number[] = generator.generateRandomNumbers(quantity);
      const uniqueNumbers: number[] = numbers.filter((value, index, array) => array.indexOf(value) === index);

      expect(numbers.length).to.equal(uniqueNumbers.length);
      expect(numbers).to.be.an('array').of.length(quantity);
    });

    it("generateRandomNumbers(20) should generate 20 random numbers", function() {
      const quantity: number = 20;
      const numbers: number[] =generator.generateRandomNumbers(quantity);
      const uniqueNumbers: number[] = numbers.filter((value, index, array) => array.indexOf(value) === index);

      expect(numbers.length).to.equal(uniqueNumbers.length);
      expect(numbers).to.be.an('array').of.length(quantity);
    });

    it("generateRandomNumbers(21) should generate 20 random numbers", function() {
      const numbers: number[] = generator.generateRandomNumbers(21);
      const uniqueNumbers: number[] = numbers.filter((value, index, array) => array.indexOf(value) === index);

      expect(numbers.length).to.equal(uniqueNumbers.length);
      expect(numbers).to.be.an('array').of.length(20);
    });

    it("generateRandomNumbers(55) should generate 20 random numbers", function() {
      const numbers: number[] = generator.generateRandomNumbers(55);
      const uniqueNumbers: number[] = numbers.filter((value, index, array) => array.indexOf(value) === index);

      expect(numbers.length).to.equal(uniqueNumbers.length);
      expect(numbers).to.be.an('array').of.length(20);
    });

    it("generateRandomNumbers(100) should generate 20 random numbers", function() {
      const numbers: number[] = generator.generateRandomNumbers(100);
      const uniqueNumbers: number[] = numbers.filter((value, index, array) => array.indexOf(value) === index);

      expect(numbers.length).to.equal(uniqueNumbers.length);
      expect(numbers).to.be.an('array').of.length(20);
    });
  });

  describe("Generates unique random numbers between 1 and 100", function() {
    it("Minimum value generated should be greater than or equal to 1", function() {
      const numbers: number[] = generator.generateRandomNumbers(20);
      const uniqueNumbers: number[] = numbers.filter((value, index, array) => array.indexOf(value) === index);
      const minValue: number = Math.min(...numbers);

      expect(numbers.length).to.equal(uniqueNumbers.length);
      expect(minValue).to.be.greaterThanOrEqual(1);
    });
  
    it("Maximum value generated should be less than or equal to 100", function() {
      const numbers: number[] = generator.generateRandomNumbers(20);
      const maxValue: number = Math.max(...numbers);
      const uniqueNumbers: number[] = numbers.filter((value, index, array) => array.indexOf(value) === index);

      expect(numbers.length).to.equal(uniqueNumbers.length);
      expect(maxValue).to.be.lessThanOrEqual(100);
    });
  });

});