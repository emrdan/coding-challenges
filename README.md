To run the project:

  1. Clone the project
  2. Install dependencies by running: `npm install`
  3. Run `npm start --help for a list of available commands`
  4. Example usage would be: `npm start random 5`